from setuptools import setup, find_packages

setup(
    name='edify-ctl',
    version='0.1.0',
    packages=find_packages(),
    include_package_data=True,
    py_modules=['edify-ctl'],
    install_requires=[
        'Click',
    ],
    entry_points='''
        [console_scripts]
        edify-ctl=edify:cli
    ''',
    author="Jose Torres",
    author_email="jose@edify.social",
    description="Background emulator manager.",
    license="Proprietary",
    keywords="emulators server monitor api",
    url="https://gitlab.randomprimate.com/Edify/emulator-automation",
)