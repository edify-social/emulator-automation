import os
import instagram_login as insta_login
import instagram_nav as insta_nav
import click
from com.dtmilano.android.viewclient import ViewClient


def start_assinged_proc():
    time_minutes = 0.2

    # Connects to the current device, returning a MonkeyDevice object
    device, serialno = ViewClient.connectToDeviceOrExit()

    user_name = os.environ["accUser"]
    user_password = os.environ["accPass"]

    click.echo("  * Initializing Instagram app")
    # Start Instagram app
    insta_package = 'com.instagram.android'
    insta_activity = 'com.instagram.android.activity.MainTabActivity'
    instaRunComponent = insta_package + '/' + insta_activity
    device.startActivity(component=instaRunComponent)

    click.echo("  * Starting log in process")
    # Log into Instagram
    insta_login.instagram_monkey_login(device, time_minutes, user_name, user_password)

    click.echo("  * Navigating through Instagram")
    # Navigate to activity screen
    insta_nav.nav_heart_follow(device)

    # Navigate to following screen
    insta_nav.nav_following_act(device, time_minutes)

    # Navigate to profile screen
    insta_nav.nav_profile(device)

    # Navigate to followers screen
    insta_nav.nav_followers(device, time_minutes)

    # Navigate to following screen
    insta_nav.nav_following(device, time_minutes)
