from com.dtmilano.android.viewclient import ViewClient
import time


#
# Going to heart and following/you screens
#
def nav_heart_follow(device):
    ViewClient.sleep(1)
    device.touch(753, 1723, 'DOWN_AND_UP')
    ViewClient.sleep(1)


#
# Select following activity screen
#
def nav_following_act(device, tm):
    device.touch(257, 130, 'DOWN_AND_UP')
    ViewClient.sleep(1)
    t_end = time.time() + 60 * tm
    while time.time() < t_end:
        device.press('KEYCODE_DPAD_DOWN')
        #device.drag((500, 1600), (500, 220), 1.0, 80)
    # Selecting the "you"
    device.touch(804, 145, 'DOWN_AND_UP')
    ViewClient.sleep(1)
    t_end = time.time() + 60 * tm
    while time.time() < t_end:
        device.press('KEYCODE_DPAD_DOWN')
        #device.drag((500, 1600), (500, 220), 1.0, 80)


#
# Going to User's profile screen
#
def nav_profile(device):
    ViewClient.sleep(1)
    device.touch(970, 1715, 'DOWN_AND_UP')
    ViewClient.sleep(1)


#
# Selecting followers
#
def nav_followers(device, tm):
    # No confirmation message
    device.touch(680, 300, 'DOWN_AND_UP')
    ViewClient.sleep(2)
    t_end = time.time() + 60 * tm
    while time.time() < t_end:
        device.press('KEYCODE_DPAD_DOWN')
        #device.drag((500, 1600), (500, 220), 1.0, 80)


#
# Selecting following
#
def nav_following(device, tm):
    ViewClient.sleep(1)
    device.touch(72, 147, 'DOWN_AND_UP')
    ViewClient.sleep(1)
    device.touch(923, 328, 'DOWN_AND_UP')
    ViewClient.sleep(1)
    t_end = time.time() + 60 * tm
    while time.time() < t_end:
        device.press('KEYCODE_DPAD_DOWN')
        #device.drag((500, 1600), (500, 220), 1.0, 80)
