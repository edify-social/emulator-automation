# Emulator constants
emulatorPort = "5554"

# Server
# hostname = get from env var after main sets it
hostname = "ip-172-31-26-23"

# Environment
# environment = 'development'

# Downloaded APK file name and path
emu_monitor_apk = "downloads/emulator-monitor.apk"
insta_apk = "downloads/com.instagram.android_9.6.7-x86.apk"
# insta_apk = "downloads/com.instagram.android_10.18.0-x86.apk"

# Download S3 bucket URI for APKs
insta_apk_uri = "https://s3-us-west-2.amazonaws.com/emulator-apks/instagram/com.instagram.android_10.3.0_arm.apk"
emu_monitor_apk_uri = "https://s3-us-west-2.amazonaws.com/emulator-apks/edify/emulator-monitor.apk"

#  API Endpoints
edify_api_url = "https://api.edify.social/"
edify_api_version = "v1/"
account_sheets_post = edify_api_url + edify_api_version + "account_sheets"
main_feed_post = edify_api_url + edify_api_version + "insta_data/main_feed"

# Dev endpoint
dev_edify_api_url = "http://api.edify.dev:3000/"
dev_edify_api_version = "v1/"
dev_account_sheets_post = dev_edify_api_url + dev_edify_api_version + "account_sheets"
dev_main_feed_post = dev_edify_api_url + dev_edify_api_version + "insta_data/main_feed"
