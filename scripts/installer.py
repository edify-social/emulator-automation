import os
import click
import vars
import subprocess
from com.dtmilano.android.viewclient import ViewClient


def installer():
    # Broadcast active script
    click.echo("")
    click.echo("------------------------------------------------------------------------------------")
    click.echo("Script installer is now running")
    click.echo("------------------------------------------------------------------------------------")

    devices = os.popen('adb devices').read().strip().split('\n')[1:]
    deviceid = devices[0].split('\t')[0]
    device, serialno = ViewClient.connectToDeviceOrExit()
    click.echo("- Devices declared")
    pls = "pop"
    # Instagram APK
    apk_path = device.shell('pm path com.instagram.android')
    if apk_path.startswith('package:'):
        click.echo("- Instagram already installed.")
    else:
        click.echo("- Instagram app is not installed, installing APK...")
        subprocess.call(["adb", "install", vars.insta_apk])
        click.echo("- Instagram app installed")

    click.echo("------------------------------------------------------------------------------------")
    click.echo("Instagram APK is now installed on device " + deviceid)
    click.echo("------------------------------------------------------------------------------------")
    click.echo("")
