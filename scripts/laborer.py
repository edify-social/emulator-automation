import json
import os
import subprocess
import time
import urllib2
import click
import vars
import scripts.account_assigned as act_assigned
import scripts.account_taken as act_taken
import scripts.downloader as edify_download
import scripts.installer as edify_install

from subprocess import call


#
# Function Declaration
#
# Downloader Script
def downloader():
    edify_download.downloader()
    return

# Installer Script
def installer_script():
    edify_install.installer()
    return

# Emulator Starter Function
def start_emu(emu_name, emu_port, wipe_data):
    click.echo("  * Starting emulator " + emu_name)
    env = os.environ['EDIFY_ENV']
    if wipe_data == 1:
        if env == 'development':
            subprocess.Popen(['emulator', '-avd', emu_name, '-port', emu_port, '-wipe-data'])
        else:
            subprocess.Popen(['/home/desktop/Android/Sdk/tools/emulator', '-avd', emu_name, '-port', emu_port, '-wipe-data'])
        os.system('adb wait-for-device')
        os.system("while [ \"`adb shell getprop sys.boot_completed | tr -d '\r' `\" != \"1\" ] ; do sleep 1; done")
        kill_emu(emu_name, emu_port)
    if env == 'development':
        subprocess.Popen(['emulator', '-avd', emu_name, '-port', emu_port])
    else:
        subprocess.Popen(['/home/desktop/Android/Sdk/tools/emulator', '-avd', emu_name, '-port', emu_port])

    os.system('adb wait-for-device')
    # Restart adb as root
    call(["adb", "root"])
    click.echo("  * Loading emulator...")
    # Don't exit until emulator is loaded
    os.system("while [ \"`adb shell getprop sys.boot_completed | tr -d '\r' `\" != \"1\" ] ; do sleep 1; done")
    click.echo("  * Emulator ready")


# Emulator killer
def kill_emu(emu_name, emu_port):
    click.echo("  * Killing " + emu_name + "...")
    what_emu = "emulator-" + emu_port
    click.echo("  * " + what_emu)
    call(["adb", "-s", what_emu, "emu", "kill"])

# Post updates to app server
def main_feed_end_point_post(raw_data, env):
    data = json.dumps(raw_data)
    clen = len(data)
    if env == 'development':
        url = vars.dev_main_feed_post
    else:
        url = vars.main_feed_post
    data = json.dumps(raw_data)
    clen = len(data)
    req = urllib2.Request(url, data, {'Content-Type': 'application/json', 'Content-Length': clen})
    f = urllib2.urlopen(req)
    post_reply = f.read()
    response = post_reply
    return response


# Request Account Sheets for this server
def get_account_sheets(env):
    data = json.dumps({
        "hostname": vars.hostname
    })
    if env == 'development':
        url = vars.dev_account_sheets_post
    else:
        url = vars.account_sheets_post
    req = urllib2.Request(url, data, {'Content-Type': 'application/json'})
    f = urllib2.urlopen(req)
    response = f.read()
    return response


# Post updates to app server
def end_point_post(raw_data, env):
    data = json.dumps(raw_data)
    clen = len(data)
    if env == 'development':
        url = vars.dev_account_sheets_post
    else:
        url = vars.account_sheets_post
    req = urllib2.Request(url, data, {'Content-Type': 'application/json', 'Content-Length': clen})
    f = urllib2.urlopen(req)
    post_reply = f.read()
    response = post_reply
    return response


# Data management between the emulator and host
# Gets files from emulator's Innstagram app and delivers them to the emulator monitor app
def chmodcppullpush(env, pin, emu):

    click.echo("- Setting root for ADB")
    # Restart adb as root
    call(["adb", "root"])

    # Get data
    call(["adb", "pull", "/data/data/com.instagram.android/cache/http_responses", "./insta-data/http_responses/emu_" + str(emu)])
    call(["adb", "pull", "/data/data/com.instagram.android/cache/direct_thread_store/direct_thread_store.clean", "./insta-data/other_users/emu_" + str(emu) + "/direct_thread_store.json"])

    # Json file to dictionary
    json_file = open("./insta-data/other_users/emu_" + str(emu) + "/direct_thread_store.json", "r")
    # SHOULD WE BE SENDING THE OTHER FILES AS WELL? SEEMS THIS ONE WOULDN'T BE COMPLETE

    parsed_json = json.load(json_file)
    json_file.close()
    parsed_json['pin'] = pin
    main_feed_end_point_post(parsed_json, env)
    return


#
# Main process
#
@click.command()
@click.option('--demo', help='If demo is True no emulator will run.', default=False)
def labor(demo):
    """
    Name: Laborer\n
    Description: Reads server endpoints and manages emulator start and kill.
    """
    # Broadcast active script
    click.echo("")
    click.echo("------------------------------------------------------------------------------------")
    click.echo("Script laborer is now running")
    click.echo("------------------------------------------------------------------------------------")
    # Set centinela ctr
    centinelaCounterEmu = 0
    # Store reply from server to account sheet endpoint
    server_acct_sheets = get_account_sheets(os.environ['EDIFY_ENV'])
    parsed_acct_sheets = json.loads(server_acct_sheets)
    updated_acct_sheets = parsed_acct_sheets

    # Check if this is still needed.
    tmp = parsed_acct_sheets

    click.echo("- Requesting updates from HQ")
    click.echo("------------------------------------------------------------------------------------")
    click.echo("Laborer main process running")
    click.echo("------------------------------------------------------------------------------------")

    # Explore JSON response from server
    for idx, rec in enumerate(parsed_acct_sheets['people']):
        # Add to counter
        # substitute counter with idx
        centinelaCounterEmu += 1
        # Set attributes for info to report back
        updated_acct_sheets["people"][idx]["last_run"] = str(time.time())
        updated_acct_sheets["people"][idx]["info"][0] = "emu_" + str(centinelaCounterEmu)

        # Validate empty pin value
        if rec['pin'] is None:
            os.environ["pin"] = 'None'
        else:
            os.environ["pin"] = rec['pin']
        # Validate empty Instagram username value
        if rec['instagram_account'][0] is None:
            os.environ["accUser"] = 'None'
        else:
            os.environ["accUser"] = rec['instagram_account'][0]
        # Validate empty Instagram password value
        if rec['instagram_account'][1] is None:
            os.environ["accPass"] = 'None'
        else:
            os.environ["accPass"] = rec['instagram_account'][1]
        # Validate empty status value
        if rec['status'] is None:
            centinelaStatus = 'None'
        else:
            centinelaStatus = rec['status']
        # Validate empty ash id value
        if rec['ash_id'] is None:
            emu_id = 'None'
        else:
            emu_id = rec['ash_id']

        # Report back that current emulator is running
        updated_acct_sheets["people"][idx]["status"] = "running"
        updated_acct_sheets["people"][idx]["info"][1] = "running"
        updated_acct_sheets["hostname"] = vars.hostname
        updated_acct_sheets["api_action"] = "update"
        end_point_post(updated_acct_sheets, os.environ['EDIFY_ENV'])

        # Print account overview
        click.echo("- Emulator ID: " + str(emu_id))
        click.echo("  * Status for account position " + str(centinelaCounterEmu) + " is " + rec['status'])

        # Check on status for each account sent
        if centinelaStatus == "none":
            updated_acct_sheets["people"][idx]["info"][1] = "none"
            updated_acct_sheets["people"][idx]["status"] = "none"
            click.echo("  * No need to run an emulator.")

        elif centinelaStatus == 'removed':
            # Wipe data. Factory reset.
            if not demo:
                start_emu(emu_name="N5X25_" + str(centinelaCounterEmu), emu_port="5554", wipe_data=1)
                updated_acct_sheets["people"][idx]["info"][1] = "none"
                updated_acct_sheets["people"][idx]["status"] = "none"
                kill_emu(emu_name="N5X25_" + str(centinelaCounterEmu), emu_port="5554")

            click.echo("  * Account removed")
            click.echo("  * Data wiped")

        elif centinelaStatus == 'assigned':
            click.echo("  * Bringing up a new AVD. AVD No. " + str(centinelaCounterEmu))
            click.echo("  * Starting process for a new user")

            # Set Env Vars
            if rec['instagram_account'][0] is None:
                os.environ["accUser"] = 'None'
            else:
                os.environ["accUser"] = rec['instagram_account'][0]
            # Validate empty Instagram password value
            if rec['instagram_account'][1] is None:
                os.environ["accPass"] = 'None'
            else:
                os.environ["accPass"] = rec['instagram_account'][1]
                # Validate empty pin value
            if rec['pin'] is None:
                os.environ["pin"] = 'None'
            else:
                os.environ["pin"] = rec['pin']

            # Main workflow
            if not demo:
                # Download APKs to host
                downloader()

                # Create data dirs
                click.echo("  * Creating data directories for this emulator")
                call(["mkdir", "insta-data/other_users/emu_" + str(centinelaCounterEmu)])
                call(["mkdir", "insta-data/http_responses/emu_" + str(centinelaCounterEmu)])

                start_emu(emu_name="N5X25_" + str(centinelaCounterEmu), emu_port="5554", wipe_data=0)
                installer_script()

                # Instagram app process
                act_assigned.start_assinged_proc()

                # Data management between the emulator and host
                chmodcppullpush(os.environ['EDIFY_ENV'], os.environ["pin"], str(centinelaCounterEmu))
                # Stop Emulator

                # Temp no kill
                kill_emu(emu_name="N5X25_" + str(centinelaCounterEmu), emu_port="5554")

                # Update status
                updated_acct_sheets["people"][idx]["info"][1] = "taken"
                updated_acct_sheets["people"][idx]["status"] = "taken"

        elif centinelaStatus == 'taken':
            click.echo("  * Lets Start AVD: " + str(centinelaCounterEmu))

            if not demo:
                # Start Emulator
                start_emu(emu_name="N5X25_" + str(centinelaCounterEmu), emu_port="5554", wipe_data=0)

                # Instagram app run with MonkeyRunner
                act_taken.nav_logged_in()

                # Data management between the emulator and host
                chmodcppullpush(os.environ['EDIFY_ENV'], os.environ["pin"], str(centinelaCounterEmu))

                # Stop Emulator
                kill_emu(emu_name="N5X25_" + str(centinelaCounterEmu), emu_port="5554")

                # Update status
                updated_acct_sheets["people"][idx]["info"][1] = "taken"
                updated_acct_sheets["people"][idx]["status"] = "taken"

        click.echo("  * Process complete: reporting back to server on emulator " + str(centinelaCounterEmu))

        # Report back to server
        updated_acct_sheets["hostname"] = vars.hostname
        updated_acct_sheets["api_action"] = "update"
        end_point_post(updated_acct_sheets, os.environ['EDIFY_ENV'])

    click.echo("------------------------------------------------------------------------------------")
    click.echo("Laborer complete.")
    click.echo("------------------------------------------------------------------------------------")
    click.echo("")
