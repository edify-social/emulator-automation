import os.path
import click
import urllib

import vars


def downloader():
    # Broadcast active script
    click.echo("")
    click.echo("------------------------------------------------------------------------------------")
    click.echo("Script downloader is now running")
    click.echo("------------------------------------------------------------------------------------")

    # Instagram APK
    if os.path.isfile(vars.insta_apk):
        click.echo("- Instagram apk already available")
    else:
        click.echo("- Downloading Instagram apk")
        urllib.urlretrieve(vars.insta_apk_uri, filename=vars.insta_apk)

    click.echo("------------------------------------------------------------------------------------")
    click.echo("Instagram APK is now available.")
    click.echo("------------------------------------------------------------------------------------")
    click.echo("")
