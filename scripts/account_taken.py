from com.dtmilano.android.viewclient import ViewClient
import instagram_nav as insta_nav
import click
import time


#
# Loged in
#
def nav_logged_in():
    timeout = 1000
    time_minutes = 0.2

    device, serialno = ViewClient.connectToDeviceOrExit()

    insta_package = 'com.instagram.android'
    insta_activity = 'com.instagram.android.activity.MainTabActivity'
    instaRunComponent = insta_package + '/' + insta_activity
    device.startActivity(component=instaRunComponent)
    ViewClient.sleep(5)
    click.echo("  * Navigating through Instagram")
    # Navigate to activity screen
    insta_nav.nav_heart_follow(device)

    # Navigate to following screen
    insta_nav.nav_following_act(device, time_minutes)

    # Navigate to profile screen
    insta_nav.nav_profile(device)

    # Navigate to followers screen
    insta_nav.nav_followers(device, time_minutes)

    # Navigate to following screen
    insta_nav.nav_following(device, time_minutes)
