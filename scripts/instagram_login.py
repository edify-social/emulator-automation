from com.dtmilano.android.viewclient import ViewClient
import click
import time


#
# Log in
#
def instagram_monkey_login(device, tm, usrname, usrpswd):
    click.echo("  * Waiting on Instagram")
    ViewClient.sleep(30)

    # New Version only (+ 10.18)
    # click.echo("  * I Have an Account Screen")
    # device.touch(500, 1000, 'DOWN_AND_UP')

    click.echo("  * Navigating to Sign in screen")
    device.touch(700, 1720, 'DOWN_AND_UP')
    ViewClient.sleep(4)

    click.echo("  * Typing account credentials")
    device.touch(480, 720, 'DOWN_AND_UP')
    device.type(usrname)
    ViewClient.sleep(1)

    device.press('KEYCODE_TAB')
    ViewClient.sleep(1)
    device.type(usrpswd)
    ViewClient.sleep(1)

    click.echo("  * Press on sign in button")
    device.touch(540, 824, 'DOWN_AND_UP')
    ViewClient.sleep(5)

    t_end = time.time() + 60 * tm
    while time.time() < t_end:
        device.drag((500, 1600), (500, 220), 1.0, 80)
