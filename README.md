# Emulator Automation Scripts

Python scripts to automate the emulators

The automation scripts are meant to manage the emulators on an Emulator Node. This implies getting 
updates and instructions from the server, starting, stoping and cleaning emulators on the server. This 
script is also in charge of sending updates to the application server.
 
## Workflow
Start the python script with supervisor
1. Main executes and starts all   


1. Request Account Sheet from server. 
2. Traverse the API
3. Work on emulators that have accounts
4. Wipe emulators without accounts
5. Report back to the app server after every emulator
6. When it completes it posts a sleep request

## Dev
* Requires Python 2.7 

### Setup
* Create a virtualenv `mkvirtualenv emuauto`
* Run `pip install -r requirements.txt`
* Android Studio or Android SDK is required
* Install the ABI:

```
echo "y" | android update sdk -a --no-ui --filter android-19,sys-img-armeabi-v7a-android-19
```

* Create an AVD (the echo for this command doesnt seem to work in OSX):
 
 ```
 echo "n" | android create avd --name Nexus5x_1 -t "android-19"
 ```

* Confirm the avd was created by running `emulator -list-avds`

## Deploy
To Deploy this code you'll need to create a zip file and upload it to the S3 butcket that hosts it.
This will allow Chef to pick it up when provisioning a server.  
  
This is the production url: https://s3-us-west-2.amazonaws.com/emulator-apks/python-auto/emu_auto.zip

## Communication with Server
* The API uses the same endpoint with a flag that switches the server's behavior.
* Update flag: We send the server the following attributes to update the emulator's profile
 
```
"hostname": "<server-hostname>",
"api_action": "update",
"people": [{
    "last_run": 643234567,    
    "status": "<running-taken-removed-or-none>
    "info": [
        "<emulator-name-such-as-emu-1>",
        "<same-as-status>"
    ]
}]        
```
* Sleep flag: This will stop the server and set a start time in x hrs

```
{ 
  "hostname": "<server-hostname>",
  "api_action": "sleep"
}
```
* No flag: This is used to request data only

```
{ 
  "hostname": "<server-hostname>"
}
```

## Android
* Emulators should have been created by the 

## Refs
* Supervisor https://www.digitalocean.com/community/tutorials/how-to-install-and-manage-supervisor-on-ubuntu-and-debian-vps
 