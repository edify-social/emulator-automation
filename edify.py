#!/usr/bin/env python

import click
import os
import subprocess
from subprocess import call
import scripts.vars as vrs
import scripts.laborer as lab


# @click.group()
# @click.option('--dev', help='Use dev settings', default=True)
def cli(dev):
    """
    Name: Edify\n
    Description: Emulator management.
    """
    click.echo("")
    click.echo("------------------------------------------------------------------------------------")
    click.echo("Edify emulator management process started")
    click.echo("------------------------------------------------------------------------------------")

    # Set constants
    if dev == 'production':
        # Set environment
        vrs.environment = 'production'
        # Set hostname
        # Temp disabled
        # vrs.hostname = os.system('hostname')

    # Created directories to tore data
    call(["mkdir", "insta-data"])
    call(["mkdir", "insta-data/other_users"])
    call(["mkdir", "insta-data/http_responses"])


cli(os.environ['EDIFY_ENV'])
lab.labor()
